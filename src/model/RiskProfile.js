/**
 * GURUVEST Mobile API
 * GURUVEST Mobile API to support mobile application operations.
 *
 * OpenAPI spec version: 1.0.0
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 *
 * OpenAPI Generator version: 4.0.0-beta3
 *
 * Do not edit the class manually.
 *
 */

(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD. Register as an anonymous module.
    define(['ApiClient'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    module.exports = factory(require('../ApiClient'));
  } else {
    // Browser globals (root is window)
    if (!root.GuruvestMobileApi) {
      root.GuruvestMobileApi = {};
    }
    root.GuruvestMobileApi.RiskProfile = factory(root.GuruvestMobileApi.ApiClient);
  }
}(this, function(ApiClient) {
  'use strict';



  /**
   * The RiskProfile model module.
   * @module model/RiskProfile
   * @version 1.0.0
   */

  /**
   * Constructs a new <code>RiskProfile</code>.
   * @alias module:model/RiskProfile
   * @class
   * @param riskRewardLevel {Number} The risk-reward level set by the user
   * @param initialAmount {Number} The user initial investment allocated to the strategy in USD
   * @param monthlyAmount {Number} The monthly investment allocated to the strategy in USD
   */
  var exports = function(riskRewardLevel, initialAmount, monthlyAmount) {
    var _this = this;

    _this['risk_reward_level'] = riskRewardLevel;
    _this['initial_amount'] = initialAmount;
    _this['monthly_amount'] = monthlyAmount;
  };

  /**
   * Constructs a <code>RiskProfile</code> from a plain JavaScript object, optionally creating a new instance.
   * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
   * @param {Object} data The plain JavaScript object bearing properties of interest.
   * @param {module:model/RiskProfile} obj Optional instance to populate.
   * @return {module:model/RiskProfile} The populated <code>RiskProfile</code> instance.
   */
  exports.constructFromObject = function(data, obj) {
    if (data) {
      obj = obj || new exports();
      if (data.hasOwnProperty('id')) {
        obj['id'] = ApiClient.convertToType(data['id'], 'String');
      }
      if (data.hasOwnProperty('user_id')) {
        obj['user_id'] = ApiClient.convertToType(data['user_id'], 'String');
      }
      if (data.hasOwnProperty('risk_level')) {
        obj['risk_level'] = ApiClient.convertToType(data['risk_level'], 'String');
      }
      if (data.hasOwnProperty('risk_reward_level')) {
        obj['risk_reward_level'] = ApiClient.convertToType(data['risk_reward_level'], 'Number');
      }
      if (data.hasOwnProperty('initial_amount')) {
        obj['initial_amount'] = ApiClient.convertToType(data['initial_amount'], 'Number');
      }
      if (data.hasOwnProperty('monthly_amount')) {
        obj['monthly_amount'] = ApiClient.convertToType(data['monthly_amount'], 'Number');
      }
      if (data.hasOwnProperty('create_date')) {
        obj['create_date'] = ApiClient.convertToType(data['create_date'], 'Date');
      }
      if (data.hasOwnProperty('update_date')) {
        obj['update_date'] = ApiClient.convertToType(data['update_date'], 'Date');
      }
      if (data.hasOwnProperty('is_complete')) {
        obj['is_complete'] = ApiClient.convertToType(data['is_complete'], 'Boolean');
      }
    }
    return obj;
  }

  /**
   * The unique internal identifier for the Risk Profile
   * @member {String} id
   */
  exports.prototype['id'] = undefined;
  /**
   * @member {String} user_id
   */
  exports.prototype['user_id'] = undefined;
  /**
   * @member {module:model/RiskProfile.RiskLevelEnum} risk_level
   */
  exports.prototype['risk_level'] = undefined;
  /**
   * The risk-reward level set by the user
   * @member {Number} risk_reward_level
   */
  exports.prototype['risk_reward_level'] = undefined;
  /**
   * The user initial investment allocated to the strategy in USD
   * @member {Number} initial_amount
   */
  exports.prototype['initial_amount'] = undefined;
  /**
   * The monthly investment allocated to the strategy in USD
   * @member {Number} monthly_amount
   */
  exports.prototype['monthly_amount'] = undefined;
  /**
   * Timestamp for the date and time that the record was created
   * @member {Date} create_date
   */
  exports.prototype['create_date'] = undefined;
  /**
   * Timestamp for the date and time that the record was last updated
   * @member {Date} update_date
   */
  exports.prototype['update_date'] = undefined;
  /**
   * Identify if the risk profile is complete or not
   * @member {Boolean} is_complete
   */
  exports.prototype['is_complete'] = undefined;


  /**
   * Allowed values for the <code>risk_level</code> property.
   * @enum {String}
   * @readonly
   */
  exports.RiskLevelEnum = {
    /**
     * value: "balanced"
     * @const
     */
    "balanced": "balanced",
    /**
     * value: "growth"
     * @const
     */
    "growth": "growth",
    /**
     * value: "defensive"
     * @const
     */
    "defensive": "defensive",
    /**
     * value: "preservation"
     * @const
     */
    "preservation": "preservation",
    /**
     * value: "high growth"
     * @const
     */
    "high growth": "high growth"  };


  return exports;
}));


