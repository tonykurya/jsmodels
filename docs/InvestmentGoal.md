# GuruvestMobileApi.InvestmentGoal

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**startingAmount** | **Number** |  | 
**monthlyAmount** | **Number** |  | [optional] 
**riskRewardLevel** | **Number** |  | 
**globalAmount** | **Number** |  | [optional] 


