# GuruvestMobileApi.PortfolioOverviewApi

All URIs are relative to *http://guruvest.io/mobile-api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getPortfolioOverviewAssets**](PortfolioOverviewApi.md#getPortfolioOverviewAssets) | **GET** /portfolio/assets | 
[**getPortfolioOverviewImpact**](PortfolioOverviewApi.md#getPortfolioOverviewImpact) | **GET** /portfolio/impact | 
[**getPortfolioOverviewPerfomance**](PortfolioOverviewApi.md#getPortfolioOverviewPerfomance) | **GET** /portfolio/performance | 
[**getPortfolioOverviewRiskProfile**](PortfolioOverviewApi.md#getPortfolioOverviewRiskProfile) | **GET** /portfolio/riskprofile | 



## getPortfolioOverviewAssets

> AssetResponse getPortfolioOverviewAssets()



Returns AssetResponse object for given portfolio (userId of user need to fetch from authentication step)

### Example

```javascript
var GuruvestMobileApi = require('guruvest_mobile_api');

var apiInstance = new GuruvestMobileApi.PortfolioOverviewApi();
var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.getPortfolioOverviewAssets(callback);
```

### Parameters

This endpoint does not need any parameter.

### Return type

[**AssetResponse**](AssetResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getPortfolioOverviewImpact

> ImpactResponse getPortfolioOverviewImpact()



Returns ImpactResponse object(userId of user need to fetch from authentication step).

### Example

```javascript
var GuruvestMobileApi = require('guruvest_mobile_api');

var apiInstance = new GuruvestMobileApi.PortfolioOverviewApi();
var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.getPortfolioOverviewImpact(callback);
```

### Parameters

This endpoint does not need any parameter.

### Return type

[**ImpactResponse**](ImpactResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getPortfolioOverviewPerfomance

> PerformanceResponse getPortfolioOverviewPerfomance()



Returns PerformanceResponse object (userId of user need to fetch from authentication step)

### Example

```javascript
var GuruvestMobileApi = require('guruvest_mobile_api');

var apiInstance = new GuruvestMobileApi.PortfolioOverviewApi();
var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.getPortfolioOverviewPerfomance(callback);
```

### Parameters

This endpoint does not need any parameter.

### Return type

[**PerformanceResponse**](PerformanceResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getPortfolioOverviewRiskProfile

> RiskProfile getPortfolioOverviewRiskProfile()



Returns risk profile object for given user (userId of user need to fetch from authentication step)

### Example

```javascript
var GuruvestMobileApi = require('guruvest_mobile_api');

var apiInstance = new GuruvestMobileApi.PortfolioOverviewApi();
var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.getPortfolioOverviewRiskProfile(callback);
```

### Parameters

This endpoint does not need any parameter.

### Return type

[**RiskProfile**](RiskProfile.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

