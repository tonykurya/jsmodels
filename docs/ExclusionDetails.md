# GuruvestMobileApi.ExclusionDetails

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**exclusionDescription** | **String** | A rich text description of the value selected | [optional] 
**exclusionHashtags** | **String** | A CSV format list of hashtags | [optional] 


