# GuruvestMobileApi.InvestmentGoalApi

All URIs are relative to *http://guruvest.io/mobile-api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getInvestmentGoal**](InvestmentGoalApi.md#getInvestmentGoal) | **GET** /goal | 
[**updateInvestmentGoal**](InvestmentGoalApi.md#updateInvestmentGoal) | **PUT** /goal | 



## getInvestmentGoal

> InvestmentGoal getInvestmentGoal()



Returns default investment details(userId of user need to fetch from authentication step)

### Example

```javascript
var GuruvestMobileApi = require('guruvest_mobile_api');

var apiInstance = new GuruvestMobileApi.InvestmentGoalApi();
var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.getInvestmentGoal(callback);
```

### Parameters

This endpoint does not need any parameter.

### Return type

[**InvestmentGoal**](InvestmentGoal.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## updateInvestmentGoal

> InvestmentGoal updateInvestmentGoal(investmentGoal)



Update investment goals in server side and return latest investment goal(userId of user need to fetch from authentication step)

### Example

```javascript
var GuruvestMobileApi = require('guruvest_mobile_api');

var apiInstance = new GuruvestMobileApi.InvestmentGoalApi();
var investmentGoal = new GuruvestMobileApi.InvestmentGoal(); // InvestmentGoal | Investment goal object that need to update.
var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.updateInvestmentGoal(investmentGoal, callback);
```

### Parameters



Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **investmentGoal** | [**InvestmentGoal**](InvestmentGoal.md)| Investment goal object that need to update. | 

### Return type

[**InvestmentGoal**](InvestmentGoal.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

