# GuruvestMobileApi.BrokersApi

All URIs are relative to *http://guruvest.io/mobile-api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getBrokers**](BrokersApi.md#getBrokers) | **GET** /brokers | 



## getBrokers

> [Broker] getBrokers()



Returns Broker object list. With this response mobile app UI can render specific UI to collect information according to selected broker.

### Example

```javascript
var GuruvestMobileApi = require('guruvest_mobile_api');

var apiInstance = new GuruvestMobileApi.BrokersApi();
var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.getBrokers(callback);
```

### Parameters

This endpoint does not need any parameter.

### Return type

[**[Broker]**](Broker.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

