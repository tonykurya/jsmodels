# GuruvestMobileApi.AccountDetailsApi

All URIs are relative to *http://guruvest.io/mobile-api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createAccount**](AccountDetailsApi.md#createAccount) | **POST** /accounts | 
[**getAccount**](AccountDetailsApi.md#getAccount) | **GET** /account_info | 
[**getAccountImage**](AccountDetailsApi.md#getAccountImage) | **GET** /accounts/{account_id}/image | 
[**getAccounts**](AccountDetailsApi.md#getAccounts) | **GET** /accounts | 
[**updateAccount**](AccountDetailsApi.md#updateAccount) | **PUT** /account_info | 
[**uploadImageToAccount**](AccountDetailsApi.md#uploadImageToAccount) | **POST** /accounts/{account_id}/image | 



## createAccount

> Account createAccount(account)



This endpoint will be used to create an Account object on the server side and return created account object.

### Example

```javascript
var GuruvestMobileApi = require('guruvest_mobile_api');

var apiInstance = new GuruvestMobileApi.AccountDetailsApi();
var account = new GuruvestMobileApi.Account(); // Account | Account object that need to create in server side.
var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.createAccount(account, callback);
```

### Parameters



Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **account** | [**Account**](Account.md)| Account object that need to create in server side. | 

### Return type

[**Account**](Account.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## getAccount

> Account getAccount(brokerName)



Returns Account object for given broker (userId of user need to fetch from authentication step)

### Example

```javascript
var GuruvestMobileApi = require('guruvest_mobile_api');

var apiInstance = new GuruvestMobileApi.AccountDetailsApi();
var brokerName = "brokerName_example"; // String | To specifically filter accountInfo by name of broker.
var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.getAccount(brokerName, callback);
```

### Parameters



Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **brokerName** | **String**| To specifically filter accountInfo by name of broker. | 

### Return type

[**Account**](Account.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getAccountImage

> File getAccountImage(accountId)



### Example

```javascript
var GuruvestMobileApi = require('guruvest_mobile_api');

var apiInstance = new GuruvestMobileApi.AccountDetailsApi();
var accountId = "accountId_example"; // String | Value identification number to retrieve image of record.
var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.getAccountImage(accountId, callback);
```

### Parameters



Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **accountId** | **String**| Value identification number to retrieve image of record. | 

### Return type

**File**

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: image/_*, application/json


## getAccounts

> [Account] getAccounts(opts)



Returns Account object list (userId of user need to fetch from authentication step)

### Example

```javascript
var GuruvestMobileApi = require('guruvest_mobile_api');

var apiInstance = new GuruvestMobileApi.AccountDetailsApi();
var opts = {
  'accountId': "accountId_example" // String | To specifically filter accounts by account id.
};
var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.getAccounts(opts, callback);
```

### Parameters



Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **accountId** | **String**| To specifically filter accounts by account id. | [optional] 

### Return type

[**[Account]**](Account.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## updateAccount

> Account updateAccount(account)



Update Account object in server side and return the updated Account object.

### Example

```javascript
var GuruvestMobileApi = require('guruvest_mobile_api');

var apiInstance = new GuruvestMobileApi.AccountDetailsApi();
var account = new GuruvestMobileApi.Account(); // Account | Account object that need to update in server side.
var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.updateAccount(account, callback);
```

### Parameters



Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **account** | [**Account**](Account.md)| Account object that need to update in server side. | 

### Return type

[**Account**](Account.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## uploadImageToAccount

> uploadImageToAccount(accountId, opts)



update image object in server side. We are adding specific resource to add API as image uploading is time consuming task which can run outside account create process.

### Example

```javascript
var GuruvestMobileApi = require('guruvest_mobile_api');

var apiInstance = new GuruvestMobileApi.AccountDetailsApi();
var accountId = "accountId_example"; // String | Value identification number to update image of record.
var opts = {
  'file': "/path/to/file" // File | 
};
var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully.');
  }
};
apiInstance.uploadImageToAccount(accountId, opts, callback);
```

### Parameters



Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **accountId** | **String**| Value identification number to update image of record. | 
 **file** | **File**|  | [optional] 

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: multipart/form-data
- **Accept**: application/json

