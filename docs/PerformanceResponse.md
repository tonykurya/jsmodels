# GuruvestMobileApi.PerformanceResponse

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**valueOfPortfolio** | **Number** |  | [optional] 
**assets** | [**[Position]**](Position.md) |  | [optional] 


