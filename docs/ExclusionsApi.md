# GuruvestMobileApi.ExclusionsApi

All URIs are relative to *http://guruvest.io/mobile-api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getExclusionById**](ExclusionsApi.md#getExclusionById) | **GET** /exclusions/{exclusion_id} | 
[**getExclusions**](ExclusionsApi.md#getExclusions) | **GET** /exclusions | 
[**updateExclusionById**](ExclusionsApi.md#updateExclusionById) | **PUT** /exclusions/{exclusion_id} | 



## getExclusionById

> Exclusion getExclusionById(exclusionId)



Returns exclusion object for given exclusion id(userId of user need to fetch from authentication step)

### Example

```javascript
var GuruvestMobileApi = require('guruvest_mobile_api');

var apiInstance = new GuruvestMobileApi.ExclusionsApi();
var exclusionId = "exclusionId_example"; // String | Exclusion identification number to retrieve record.
var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.getExclusionById(exclusionId, callback);
```

### Parameters



Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **exclusionId** | **String**| Exclusion identification number to retrieve record. | 

### Return type

[**Exclusion**](Exclusion.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getExclusions

> [Exclusion] getExclusions(opts)



Returns exclusion object list(userId of user need to fetch from authentication step)

### Example

```javascript
var GuruvestMobileApi = require('guruvest_mobile_api');

var apiInstance = new GuruvestMobileApi.ExclusionsApi();
var opts = {
  'exclusionName': "exclusionName_example", // String | To specifically filter exclusions by its name.
  'isSelected': true // Boolean | To specifically filter selected or not selected exclusion(selecyed by user).
};
var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.getExclusions(opts, callback);
```

### Parameters



Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **exclusionName** | **String**| To specifically filter exclusions by its name. | [optional] 
 **isSelected** | **Boolean**| To specifically filter selected or not selected exclusion(selecyed by user). | [optional] 

### Return type

[**[Exclusion]**](Exclusion.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## updateExclusionById

> Exclusion updateExclusionById(exclusionId, exclusion)



Update exclusion object in server side and return exclusion object which updated(userId of user need to fetch from authentication step)

### Example

```javascript
var GuruvestMobileApi = require('guruvest_mobile_api');

var apiInstance = new GuruvestMobileApi.ExclusionsApi();
var exclusionId = "exclusionId_example"; // String | Exclusion identification number to update record
var exclusion = new GuruvestMobileApi.Exclusion(); // Exclusion | Exclusion object that needs to be updated on the server side.
var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.updateExclusionById(exclusionId, exclusion, callback);
```

### Parameters



Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **exclusionId** | **String**| Exclusion identification number to update record | 
 **exclusion** | [**Exclusion**](Exclusion.md)| Exclusion object that needs to be updated on the server side. | 

### Return type

[**Exclusion**](Exclusion.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

