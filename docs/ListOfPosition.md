# GuruvestMobileApi.ListOfPosition

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**assetClass** | **String** | The asset class name, for example \&quot;us_equity\&quot; | [optional] 
**assets** | [**[Position]**](Position.md) |  | [optional] 


