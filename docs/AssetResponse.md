# GuruvestMobileApi.AssetResponse

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**valueOfPortfolio** | **Number** |  | [optional] 
**assets** | [**[ListOfPosition]**](ListOfPosition.md) |  | [optional] 


