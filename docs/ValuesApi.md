# GuruvestMobileApi.ValuesApi

All URIs are relative to *http://guruvest.io/mobile-api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getValueById**](ValuesApi.md#getValueById) | **GET** /values/{value_id} | 
[**getValues**](ValuesApi.md#getValues) | **GET** /values | 
[**updateValueById**](ValuesApi.md#updateValueById) | **PUT** /values/{value_id} | 



## getValueById

> Value getValueById(valueId)



Returns value object for given value id(userId of user need to fetch from authentication step)

### Example

```javascript
var GuruvestMobileApi = require('guruvest_mobile_api');

var apiInstance = new GuruvestMobileApi.ValuesApi();
var valueId = "valueId_example"; // String | Value identification number to retrieve record.
var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.getValueById(valueId, callback);
```

### Parameters



Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **valueId** | **String**| Value identification number to retrieve record. | 

### Return type

[**Value**](Value.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getValues

> [Value] getValues(opts)



Returns value object list(userId of user need to fetch from authentication step)

### Example

```javascript
var GuruvestMobileApi = require('guruvest_mobile_api');

var apiInstance = new GuruvestMobileApi.ValuesApi();
var opts = {
  'valueName': "valueName_example", // String | To specifically filter values by its name.
  'isSelected': true // Boolean | To specifically filter selected or not selected value(selecyed by user).
};
var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.getValues(opts, callback);
```

### Parameters



Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **valueName** | **String**| To specifically filter values by its name. | [optional] 
 **isSelected** | **Boolean**| To specifically filter selected or not selected value(selecyed by user). | [optional] 

### Return type

[**[Value]**](Value.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## updateValueById

> Value updateValueById(valueId, value)



Update value object in server side and return value object which updated(userId of user need to fetch from authentication step)

### Example

```javascript
var GuruvestMobileApi = require('guruvest_mobile_api');

var apiInstance = new GuruvestMobileApi.ValuesApi();
var valueId = "valueId_example"; // String | Value identification number to update record
var value = new GuruvestMobileApi.Value(); // Value | Value object that needs to be updated on the server side.
var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.updateValueById(valueId, value, callback);
```

### Parameters



Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **valueId** | **String**| Value identification number to update record | 
 **value** | [**Value**](Value.md)| Value object that needs to be updated on the server side. | 

### Return type

[**Value**](Value.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

