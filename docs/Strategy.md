# GuruvestMobileApi.Strategy

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**strategyId** | **String** | The identifier of the strategy | [optional] 
**isActive** | **Boolean** |  | [optional] 
**riskLevel** | **Number** |  | [optional] 
**potentialReturn** | **Number** |  | [optional] 
**potentialLost** | **Number** |  | [optional] 
**expectedReturn** | **Number** |  | [optional] 
**expectedAssets** | **Number** |  | [optional] 


