# GuruvestMobileApi.PersonalInformation

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**propertyName** | **String** |  | [optional] 
**propertyValue** | **String** |  | [optional] 


