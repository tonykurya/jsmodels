/**
 * GURUVEST Mobile API
 * GURUVEST Mobile API to support mobile application operations.
 *
 * OpenAPI spec version: 1.0.0
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 *
 * OpenAPI Generator version: 4.0.0-beta3
 *
 * Do not edit the class manually.
 *
 */

(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD. Register as an anonymous module.
    define(['ApiClient', 'model/ErrorModel', 'model/Exclusion'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    module.exports = factory(require('../ApiClient'), require('../model/ErrorModel'), require('../model/Exclusion'));
  } else {
    // Browser globals (root is window)
    if (!root.GuruvestMobileApi) {
      root.GuruvestMobileApi = {};
    }
    root.GuruvestMobileApi.ExclusionsApi = factory(root.GuruvestMobileApi.ApiClient, root.GuruvestMobileApi.ErrorModel, root.GuruvestMobileApi.Exclusion);
  }
}(this, function(ApiClient, ErrorModel, Exclusion) {
  'use strict';

  /**
   * Exclusions service.
   * @module api/ExclusionsApi
   * @version 1.0.0
   */

  /**
   * Constructs a new ExclusionsApi. 
   * @alias module:api/ExclusionsApi
   * @class
   * @param {module:ApiClient} [apiClient] Optional API client implementation to use,
   * default to {@link module:ApiClient#instance} if unspecified.
   */
  var exports = function(apiClient) {
    this.apiClient = apiClient || ApiClient.instance;


    /**
     * Callback function to receive the result of the getExclusionById operation.
     * @callback module:api/ExclusionsApi~getExclusionByIdCallback
     * @param {String} error Error message, if any.
     * @param {module:model/Exclusion} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Returns exclusion object for given exclusion id(userId of user need to fetch from authentication step)
     * @param {String} exclusionId Exclusion identification number to retrieve record.
     * @param {module:api/ExclusionsApi~getExclusionByIdCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link module:model/Exclusion}
     */
    this.getExclusionById = function(exclusionId, callback) {
      var postBody = null;

      // verify the required parameter 'exclusionId' is set
      if (exclusionId === undefined || exclusionId === null) {
        throw new Error("Missing the required parameter 'exclusionId' when calling getExclusionById");
      }


      var pathParams = {
        'exclusion_id': exclusionId
      };
      var queryParams = {
      };
      var collectionQueryParams = {
      };
      var headerParams = {
      };
      var formParams = {
      };

      var authNames = [];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = Exclusion;
      return this.apiClient.callApi(
        '/exclusions/{exclusion_id}', 'GET',
        pathParams, queryParams, collectionQueryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, null, callback
      );
    }

    /**
     * Callback function to receive the result of the getExclusions operation.
     * @callback module:api/ExclusionsApi~getExclusionsCallback
     * @param {String} error Error message, if any.
     * @param {Array.<module:model/Exclusion>} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Returns exclusion object list(userId of user need to fetch from authentication step)
     * @param {Object} opts Optional parameters
     * @param {String} opts.exclusionName To specifically filter exclusions by its name.
     * @param {Boolean} opts.isSelected To specifically filter selected or not selected exclusion(selecyed by user).
     * @param {module:api/ExclusionsApi~getExclusionsCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link Array.<module:model/Exclusion>}
     */
    this.getExclusions = function(opts, callback) {
      opts = opts || {};
      var postBody = null;


      var pathParams = {
      };
      var queryParams = {
        'exclusion_name': opts['exclusionName'],
        'is_selected': opts['isSelected'],
      };
      var collectionQueryParams = {
      };
      var headerParams = {
      };
      var formParams = {
      };

      var authNames = [];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = [Exclusion];
      return this.apiClient.callApi(
        '/exclusions', 'GET',
        pathParams, queryParams, collectionQueryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, null, callback
      );
    }

    /**
     * Callback function to receive the result of the updateExclusionById operation.
     * @callback module:api/ExclusionsApi~updateExclusionByIdCallback
     * @param {String} error Error message, if any.
     * @param {module:model/Exclusion} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Update exclusion object in server side and return exclusion object which updated(userId of user need to fetch from authentication step)
     * @param {String} exclusionId Exclusion identification number to update record
     * @param {module:model/Exclusion} exclusion Exclusion object that needs to be updated on the server side.
     * @param {module:api/ExclusionsApi~updateExclusionByIdCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link module:model/Exclusion}
     */
    this.updateExclusionById = function(exclusionId, exclusion, callback) {
      var postBody = exclusion;

      // verify the required parameter 'exclusionId' is set
      if (exclusionId === undefined || exclusionId === null) {
        throw new Error("Missing the required parameter 'exclusionId' when calling updateExclusionById");
      }

      // verify the required parameter 'exclusion' is set
      if (exclusion === undefined || exclusion === null) {
        throw new Error("Missing the required parameter 'exclusion' when calling updateExclusionById");
      }


      var pathParams = {
        'exclusion_id': exclusionId
      };
      var queryParams = {
      };
      var collectionQueryParams = {
      };
      var headerParams = {
      };
      var formParams = {
      };

      var authNames = [];
      var contentTypes = ['application/json'];
      var accepts = ['application/json'];
      var returnType = Exclusion;
      return this.apiClient.callApi(
        '/exclusions/{exclusion_id}', 'PUT',
        pathParams, queryParams, collectionQueryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, null, callback
      );
    }
  };

  return exports;
}));
