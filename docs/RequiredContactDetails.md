# GuruvestMobileApi.RequiredContactDetails

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**propertyName** | **String** |  | [optional] 
**propertyType** | **String** |  | [optional] 


