# GuruvestMobileApi.ValueDetails

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**valueDescription** | **String** | A rich text description of the value selected | [optional] 
**valueHashtags** | **String** | A CSV format list of hashtags | [optional] 


