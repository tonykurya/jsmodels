# GuruvestMobileApi.Account

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**accountId** | **String** | The identifier of the linked account | 
**brokerName** | **String** |  | [optional] 
**contactDetails** | [**[ContactDetails]**](ContactDetails.md) |  | [optional] 
**personalInformation** | [**[PersonalInformation]**](PersonalInformation.md) |  | [optional] 
**accountDetails** | [**AccountDetails**](AccountDetails.md) |  | [optional] 
**assetsAllocated** | **Number** |  | [optional] 
**status** | **String** |  | [optional] 
**type** | **String** |  | [optional] 



## Enum: StatusEnum


* `active` (value: `"active"`)

* `not_active` (value: `"not_active"`)

* `pending` (value: `"pending"`)





## Enum: TypeEnum


* `trading` (value: `"trading"`)

* `saving` (value: `"saving"`)




