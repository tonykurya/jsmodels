# GuruvestMobileApi.Position

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**assetId** | **String** |  | 
**assetClass** | **String** | The asset class name, for example \&quot;us_equity\&quot; | [optional] 
**marketValue** | **Number** |  | [optional] 
**currentBalance** | **Number** |  | [optional] 


