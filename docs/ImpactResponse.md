# GuruvestMobileApi.ImpactResponse

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**weightedSdgImpact** | **String** |  | [optional] 
**weightedEsg** | **String** |  | [optional] 
**dimensionEnvironmental** | **String** |  | [optional] 
**dimensionSocial** | **String** |  | [optional] 
**dimensionGovernance** | **String** |  | [optional] 
**assets** | [**[ListOfPosition]**](ListOfPosition.md) |  | [optional] 


