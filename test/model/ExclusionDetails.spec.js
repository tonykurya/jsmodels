/**
 * GURUVEST Mobile API
 * GURUVEST Mobile API to support mobile application operations.
 *
 * OpenAPI spec version: 1.0.0
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 *
 * OpenAPI Generator version: 4.0.0-beta3
 *
 * Do not edit the class manually.
 *
 */

(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD.
    define(['expect.js', process.cwd()+'/src/index'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    factory(require('expect.js'), require(process.cwd()+'/src/index'));
  } else {
    // Browser globals (root is window)
    factory(root.expect, root.GuruvestMobileApi);
  }
}(this, function(expect, GuruvestMobileApi) {
  'use strict';

  var instance;

  beforeEach(function() {
    instance = new GuruvestMobileApi.ExclusionDetails();
  });

  var getProperty = function(object, getter, property) {
    // Use getter method if present; otherwise, get the property directly.
    if (typeof object[getter] === 'function')
      return object[getter]();
    else
      return object[property];
  }

  var setProperty = function(object, setter, property, value) {
    // Use setter method if present; otherwise, set the property directly.
    if (typeof object[setter] === 'function')
      object[setter](value);
    else
      object[property] = value;
  }

  describe('ExclusionDetails', function() {
    it('should create an instance of ExclusionDetails', function() {
      // uncomment below and update the code to test ExclusionDetails
      //var instance = new GuruvestMobileApi.ExclusionDetails();
      //expect(instance).to.be.a(GuruvestMobileApi.ExclusionDetails);
    });

    it('should have the property exclusionDescription (base name: "exclusion_description")', function() {
      // uncomment below and update the code to test the property exclusionDescription
      //var instance = new GuruvestMobileApi.ExclusionDetails();
      //expect(instance).to.be();
    });

    it('should have the property exclusionHashtags (base name: "exclusion_hashtags")', function() {
      // uncomment below and update the code to test the property exclusionHashtags
      //var instance = new GuruvestMobileApi.ExclusionDetails();
      //expect(instance).to.be();
    });

  });

}));
