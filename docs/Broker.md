# GuruvestMobileApi.Broker

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**brokerId** | **String** | The identifier of the broker. | 
**brokerName** | **String** |  | [optional] 
**countries** | **String** | The list of countries supportd by the broker separated by comma. Country code using the ISO 3166-1 alpha-2 | [optional] 
**requiredPersonalInformation** | [**[RequiredPersonalInformation]**](RequiredPersonalInformation.md) |  | [optional] 
**requiredContactDetails** | [**[RequiredContactDetails]**](RequiredContactDetails.md) |  | [optional] 


