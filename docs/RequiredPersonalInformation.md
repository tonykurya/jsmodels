# GuruvestMobileApi.RequiredPersonalInformation

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**propertyName** | **String** |  | [optional] 
**propertyType** | **String** |  | [optional] 


