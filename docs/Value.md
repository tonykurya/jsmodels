# GuruvestMobileApi.Value

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**valueId** | **String** | Identify the value, like sdg1, sdg2, themeX, etc... | 
**isSelected** | **Boolean** |  | [optional] 
**valueName** | **String** | The official name of the value or theme | [optional] 
**valueDetails** | [**ValueDetails**](ValueDetails.md) |  | [optional] 


