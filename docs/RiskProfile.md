# GuruvestMobileApi.RiskProfile

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** | The unique internal identifier for the Risk Profile | [optional] 
**userId** | **String** |  | [optional] 
**riskLevel** | **String** |  | [optional] 
**riskRewardLevel** | **Number** | The risk-reward level set by the user | 
**initialAmount** | **Number** | The user initial investment allocated to the strategy in USD | 
**monthlyAmount** | **Number** | The monthly investment allocated to the strategy in USD | 
**createDate** | **Date** | Timestamp for the date and time that the record was created | [optional] 
**updateDate** | **Date** | Timestamp for the date and time that the record was last updated | [optional] 
**isComplete** | **Boolean** | Identify if the risk profile is complete or not | [optional] 



## Enum: RiskLevelEnum


* `balanced` (value: `"balanced"`)

* `growth` (value: `"growth"`)

* `defensive` (value: `"defensive"`)

* `preservation` (value: `"preservation"`)

* `high growth` (value: `"high growth"`)




