# GuruvestMobileApi.AccountDetails

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**isEnabled** | **Boolean** | Is specific account is enabled or not by user. | [optional] 
**instrumentsAvailable** | **Number** | Number of instruments available for this account. | [optional] 
**totalInstruments** | **Number** | Total number of instruments available globally. | [optional] 
**totalAssetsAvailable** | **Number** | The total amount of assets available on this account. They can be part of the strategy or not. | [optional] 
**assetClasses** | **String** | The asset classes supported by the account. For example \&quot;us_equity\&quot;, \&quot;ETF\&quot;, etc... | [optional] 


