# GuruvestMobileApi.StrategyApi

All URIs are relative to *http://guruvest.io/mobile-api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getStrategy**](StrategyApi.md#getStrategy) | **GET** /strategy | 
[**updateStrategy**](StrategyApi.md#updateStrategy) | **PUT** /strategy | 



## getStrategy

> Strategy getStrategy(opts)



Returns Strategy object (userId of user need to fetch from authentication step)

### Example

```javascript
var GuruvestMobileApi = require('guruvest_mobile_api');

var apiInstance = new GuruvestMobileApi.StrategyApi();
var opts = {
  'strategyId': "strategyId_example" // String | Strategy identification number to filter by
};
var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.getStrategy(opts, callback);
```

### Parameters



Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **strategyId** | **String**| Strategy identification number to filter by | [optional] 

### Return type

[**Strategy**](Strategy.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## updateStrategy

> Strategy updateStrategy(strategy)



Update strategy object in server side and return strategy object which updated in server side (userId of user need to fetch from authentication step)

### Example

```javascript
var GuruvestMobileApi = require('guruvest_mobile_api');

var apiInstance = new GuruvestMobileApi.StrategyApi();
var strategy = new GuruvestMobileApi.Strategy(); // Strategy | strategy object that need to update in server side.
var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.updateStrategy(strategy, callback);
```

### Parameters



Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **strategy** | [**Strategy**](Strategy.md)| strategy object that need to update in server side. | 

### Return type

[**Strategy**](Strategy.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

