# GuruvestMobileApi.Exclusion

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**exclusionId** | **String** | Identify the exclusion like no_animal_test, etc... | 
**isSelected** | **Boolean** |  | [optional] 
**exclusionName** | **String** | The official name of the exclusion or theme | [optional] 
**exclusionDetails** | [**ExclusionDetails**](ExclusionDetails.md) |  | [optional] 


