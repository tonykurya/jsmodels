/**
 * GURUVEST Mobile API
 * GURUVEST Mobile API to support mobile application operations.
 *
 * OpenAPI spec version: 1.0.0
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 *
 * OpenAPI Generator version: 4.0.0-beta3
 *
 * Do not edit the class manually.
 *
 */

(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD.
    define(['expect.js', process.cwd()+'/src/index'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    factory(require('expect.js'), require(process.cwd()+'/src/index'));
  } else {
    // Browser globals (root is window)
    factory(root.expect, root.GuruvestMobileApi);
  }
}(this, function(expect, GuruvestMobileApi) {
  'use strict';

  var instance;

  beforeEach(function() {
    instance = new GuruvestMobileApi.Strategy();
  });

  var getProperty = function(object, getter, property) {
    // Use getter method if present; otherwise, get the property directly.
    if (typeof object[getter] === 'function')
      return object[getter]();
    else
      return object[property];
  }

  var setProperty = function(object, setter, property, value) {
    // Use setter method if present; otherwise, set the property directly.
    if (typeof object[setter] === 'function')
      object[setter](value);
    else
      object[property] = value;
  }

  describe('Strategy', function() {
    it('should create an instance of Strategy', function() {
      // uncomment below and update the code to test Strategy
      //var instance = new GuruvestMobileApi.Strategy();
      //expect(instance).to.be.a(GuruvestMobileApi.Strategy);
    });

    it('should have the property strategyId (base name: "strategy_id")', function() {
      // uncomment below and update the code to test the property strategyId
      //var instance = new GuruvestMobileApi.Strategy();
      //expect(instance).to.be();
    });

    it('should have the property isActive (base name: "is_active")', function() {
      // uncomment below and update the code to test the property isActive
      //var instance = new GuruvestMobileApi.Strategy();
      //expect(instance).to.be();
    });

    it('should have the property riskLevel (base name: "risk_level")', function() {
      // uncomment below and update the code to test the property riskLevel
      //var instance = new GuruvestMobileApi.Strategy();
      //expect(instance).to.be();
    });

    it('should have the property potentialReturn (base name: "potential_return")', function() {
      // uncomment below and update the code to test the property potentialReturn
      //var instance = new GuruvestMobileApi.Strategy();
      //expect(instance).to.be();
    });

    it('should have the property potentialLost (base name: "potential_lost")', function() {
      // uncomment below and update the code to test the property potentialLost
      //var instance = new GuruvestMobileApi.Strategy();
      //expect(instance).to.be();
    });

    it('should have the property expectedReturn (base name: "expected_return")', function() {
      // uncomment below and update the code to test the property expectedReturn
      //var instance = new GuruvestMobileApi.Strategy();
      //expect(instance).to.be();
    });

    it('should have the property expectedAssets (base name: "expected_assets")', function() {
      // uncomment below and update the code to test the property expectedAssets
      //var instance = new GuruvestMobileApi.Strategy();
      //expect(instance).to.be();
    });

  });

}));
